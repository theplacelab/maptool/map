const jwt = require('jsonwebtoken');
const { RESPONSE } = require('../constants.js');

module.exports.validate = function (req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    jwt.verify(
      bearerToken,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          res.sendStatus(RESPONSE.NOT_AUTHORIZED);
        } else {
          req.token = decodedToken;
          next();
        }
      }
    );
  } else {
    res.sendStatus(RESPONSE.NOT_AUTHORIZED);
  }
};

module.exports.validateWithFallback = function (
  req,
  res,
  onValidate,
  onFallback
) {
  const bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    jwt.verify(
      bearerToken,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err) => {
        if (err) {
          onFallback();
        } else {
          onValidate();
        }
      }
    );
  } else {
    onFallback();
  }
};
