const generator = require('generate-password');
let jwt = require('jsonwebtoken');
const { RESPONSE } = require('../constants.js');

module.exports = {
  requireValidToken: (req, res, onValid, onInvalid) => {
    if (Object.keys(req?.headers).includes('authorization')) {
      let token = req.headers.authorization.split(' ')[1];
      if (!token) res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      jwt.verify(
        token,
        process.env.JWT_SECRET,
        { ignoreExpiration: false },
        (err, decodedToken) => {
          if (err) {
            if (typeof onInvalid === 'function') {
              onInvalid();
            } else {
              res.sendStatus(RESPONSE.NOT_AUTHORIZED);
            }
          } else {
            req.token = decodedToken;
            onValid(decodedToken.role, decodedToken.id);
          }
        }
      );
    } else {
      if (typeof onInvalid === 'function') {
        onInvalid(decodedToken.role);
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    }
  },
  requireMinimumRole: function (requiredRole, req, res, onValid) {
    module.exports.requireValidToken(req, res, (tokenRole) => {
      if (ROLE[tokenRole] >= ROLE[requiredRole]) {
        onValid(tokenRole);
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    });
  },
  requireMinimumRoleOrSelf: function (id, requiredRole, req, res, onValid) {
    module.exports.requireValidToken(req, res, (tokenRole, tokenId) => {
      const tokenIdMatchesRequestedId = id === parseInt(tokenId, 10);
      if (ROLE[tokenRole] >= ROLE[requiredRole] || tokenIdMatchesRequestedId) {
        onValid(tokenRole);
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    });
  },
  requireSelf: function (id, req, res, onValid) {
    module.exports.requireValidToken(req, res, (tokenRole, tokenId) => {
      const tokenIdMatchesRequestedId = id === parseInt(tokenId, 10);
      if (tokenIdMatchesRequestedId) {
        onValid(tokenRole);
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    });
  },
  requireValidRefreshToken: (req, res, onValid) => {
    if (Object.keys(req?.headers).includes('authorization')) {
      let token = req.headers.authorization.split(' ')[1];
      const decoded = jwt.decode(token, { complete: true })?.payload;
      if (!decoded || !Object.keys(decoded)?.includes('id')) {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
        return;
      }
      req.knex
        .select()
        .from('users')
        .where({ id: decoded.id })
        .then(([user]) => {
          if (!token) res.sendStatus(RESPONSE.NOT_AUTHORIZED);
          jwt.verify(
            token,
            user?.refresh_secret,
            { ignoreExpiration: false },
            (err, decodedToken) => {
              if (err) {
                res.sendStatus(RESPONSE.NOT_AUTHORIZED);
              } else {
                onValid({
                  ...user,
                  extend: decodedToken.extended
                });
              }
            }
          );
        });
    } else {
      res.sendStatus(RESPONSE.NOT_AUTHORIZED);
    }
  },
  requireInBody: (params, req, res, onValid) => {
    let failedOn;
    params.forEach((param) => {
      if (
        !Object.keys(req.body).includes(param) ||
        req[param]?.trim().length === 0
      )
        failedOn = param;
    });
    if (failedOn)
      return res
        .status(RESPONSE.BAD_REQUEST)
        .send({ error: `${failedOn} required` });
    onValid();
  },
  requireInParams: (params, req, res, onValid) => {
    let failedOn;
    params.forEach((param) => {
      if (!req.params[param] || req.params[param]?.trim().length === 0)
        failedOn = param;
    });
    if (failedOn)
      return res
        .status(RESPONSE.BAD_REQUEST)
        .send({ error: `${failedOn} required` });
    onValid();
  },
  requireIntInParams: (params, req, res, onValid) => {
    let failedOn;
    params.forEach((param) => {
      if (
        !req.params[param] ||
        req.params[param]?.trim().length === 0 ||
        isNaN(parseInt(req.params[param], 10))
      ) {
        failedOn = param;
      } else {
        req.params[param] = parseInt(req.params[param], 10);
      }
    });
    if (failedOn) {
      console.log(`ERROR: ${failedOn} required`);
      return res
        .status(RESPONSE.BAD_REQUEST)
        .send({ error: `${failedOn} required` });
    }
    onValid();
  },
  requireValidPostedValidationToken: (req, res, onValid) => {
    const { token } = req.body;
    if (token) {
      const decoded = jwt.decode(token, { complete: true }).payload;
      if (!decoded.email) {
        res.sendStatus(RESPONSE.GONE);
        return;
      }
      const { email } = decoded;
      req.knex
        .select()
        .from('users')
        .where({ email })
        .then(([user]) => {
          jwt.verify(
            token,
            user?.refresh_secret,
            { ignoreExpiration: false },
            (err, decodedToken) => {
              if (err) {
                res.sendStatus(RESPONSE.GONE);
              } else {
                onValid(email);
              }
            }
          );
        });
    } else {
      res.sendStatus(RESPONSE.GONE);
    }
  },
  decodeToken: (reqOrToken, onSuccess, onFail) => {
    let token;
    if (reqOrToken.headers) {
      const bearerHeader = reqOrToken.headers['authorization'];
      if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        token = bearer[1];
      }
    } else {
      token = reqOrToken;
    }
    jwt.verify(
      token,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          if (typeof onFail === 'function') {
            onFail(err);
          } else {
            console.debug(err);
          }
        } else {
          onSuccess(decodedToken);
        }
      }
    );
  },
  generateValidationLinkFor: async (knex, email) => {
    const refresh_secret = generator.generate({
      length: 40,
      numbers: true
    });
    await knex('users').where({ email }).update({ refresh_secret, f_valid: 0 });
    const validationToken = jwt.sign(
      {
        email,
        exp: Math.floor(Date.now() / 1000) + 1440 * 60 //24hrs
      },
      refresh_secret
    );
    return `${process.env.MAIL_VALIDATION_ENDPOINT}/${validationToken}`;
  }
};
