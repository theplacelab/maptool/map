const router = require('express').Router();
const displayName = require('../package.json').displayName;

router.log = (msg) => {
  if (process.env.DEBUG?.toLowerCase() === 'true')
    console.log(`${displayName} ${msg}`);
};

router.all('*', (req, res, next) => {
  if (process.env.DEBUG && req.params[0] !== '/up') {
    console.log(`REQ: ${req.params[0]}`);
  }
  next();
});

module.exports = router;
