const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex,
  settingsVersionNumber
} = require('../testSupport.js');
const { debug } = require('console');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});
describe(`API: Settings UPDATE`, () => {
  test('Update global settings', (done) => {
    const version = `maptool_v99999`;
    server.start(() => {
      request(server.app)
        .patch('/settings')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .send({ version })
        .then(() => {
          request(server.app)
            .get('/settings/')
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then((response) => {
              const settings = JSON.parse(response.text);
              expect(response.statusCode).toEqual(RESPONSE.OK);
              expect(settings.version).toBe(version);
              done();
            });
        });
    }, knex);
  });

  test('Update map settings', (done) => {
    const version = `map_v99999`;
    server.start(() => {
      request(server.app)
        .patch('/settings/map/1')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .send({ version })
        .then(() => {
          request(server.app)
            .get('/settings/map/1')
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then((response) => {
              const settings = JSON.parse(response.text);
              expect(response.statusCode).toEqual(RESPONSE.OK);
              expect(settings.version).toBe(version);
              done();
            });
        });
    }, knex);
  });

  test('Update map settings with null should fail gracefully', (done) => {
    const version = `map_v99999`;
    server.start(() => {
      request(server.app)
        .patch(`/settings/map/${null}`)
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .send({ version })
        .then((response) => {
          expect(response.statusCode).toBe(RESPONSE.BAD_REQUEST);
          done();
        });
    }, knex);
  });

  test('Update poi settings', (done) => {
    const version = `poi_v99999`;
    server.start(() => {
      request(server.app)
        .patch('/settings/map/1/poi')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .send({ version })
        .then(() => {
          request(server.app)
            .get('/settings/map/1/poi')
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then((response) => {
              const settings = JSON.parse(response.text);
              expect(response.statusCode).toEqual(RESPONSE.OK);
              expect(settings.version).toBe(version);
              done();
            });
        });
    }, knex);
  });
});
