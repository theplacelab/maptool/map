const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: Get Map(s)`, () => {
  test(' Get maps should return maps', (done) => {
    server.start(() => {
      request(server.app)
        .get('/maps')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(Array.isArray(JSON.parse(response.text)));
          done();
        });
    }, knex);
  });
});

describe(`API: Get Map`, () => {
  test(' Get map with no id should 404', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          try {
            expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });

  test(' Get map/:id with invalid ID should 404', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map/1000')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          try {
            expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });

  test(' Get map/:id with valid ID should return map', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map/0')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          try {
            expect(response.statusCode).toEqual(RESPONSE.OK);
            expect(Array.isArray(JSON.parse(response.text)));
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });
});
