const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { debug } = require('console');
const settingsVersionNumber = '1.0';
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});
describe(`API: Settings READ`, () => {
  test('Get settings should get global settings', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .get('/settings/')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const settings = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(settings.version).toBe(`maptool_v${settingsVersionNumber}`);
          done();
        });
    }, knex);
  });

  test('Get map settings should get map settings', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .get('/settings/map/1')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const settings = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(settings.version).toBe(`map_v${settingsVersionNumber}`);
          done();
        });
    }, knex);
  });

  test('Get poi settings should get poi settings', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .get('/settings/map/1/poi')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const settings = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(settings.version).toBe(`poi_v${settingsVersionNumber}`);
          done();
        });
    }, knex);
  });
});
