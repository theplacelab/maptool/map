const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  testPoi,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { debug } = require('console');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: POI DELETE`, () => {
  const bearerToken = `bearer ${generateTokenFor(testUsers[0])}`;
  test('Delete poi/:id should delete a poi', (done) => {
    server.start(() => {
      request(server.app)
        .put('/map/1/poi')
        .send({ ...testPoi[0] })
        .set('Authorization', bearerToken)
        .then((response) => {
          const newRecord = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.CREATED);
          expect(newRecord.label).toBe(testPoi[0].label);
          request(server.app)
            .del(`/map/${newRecord.map_id}/poi/${newRecord.id}`)
            .set('Authorization', bearerToken)
            .then((response) => {
              expect(response.statusCode).toEqual(RESPONSE.OK);
              request(server.app)
                .get(`/map/${newRecord.map_id}/poi/${newRecord.id}`)
                .set('Authorization', bearerToken)
                .then((response) => {
                  expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
                  done();
                });
            });
        });
    }, knex);
  });
});
