const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  testPoi,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { debug } = require('console');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: POI_DATA Update`, () => {
  test('Update poi_data should update a poi datum', (done) => {
    const value = faker.random.words();
    const newValue = faker.random.words();
    server.start(() => {
      request(server.app)
        .put('/poi_data')
        .send({
          poi_id: '1',
          poi_data_options_id: '1',
          poi_data_format_id: '1',
          value,
          is_published: true,
          is_locked: true,
          is_markdown: true
        })
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const newRecord = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.CREATED);
          expect(newRecord.value).toBe(value);
          request(server.app)
            .patch(`/poi_data/${newRecord.id}`)
            .send({
              value: newValue
            })
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then((response) => {
              const patchRecord = JSON.parse(response.text);
              expect(response.statusCode).toEqual(RESPONSE.OK);
              expect(patchRecord.value).toBe(newValue);
              done();
            });
        });
    }, knex);
  });
});
