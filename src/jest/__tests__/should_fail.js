const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: NO TOKEN, these should all fail`, () => {
  test('Update map/:id should 401', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .patch('/map/0')
        .send({ title })
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Get map with no id should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Put map/:id should 401', (done) => {
    server.start(() => {
      request(server.app)
        .put('/map')
        .send({ title: faker.address.country() })
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Delete map/:id should 401', (done) => {
    server.start(() => {
      request(server.app)
        .del('/map/0')
        .send({ id: 0 })
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  /*
  test('Get maps should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/maps')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });
 
  test('Get map/:id should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map/1')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Get maps should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/maps')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Get settings should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/settings')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Get settings/map/1 should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/settings/map/1')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });
  
  test('Get settings/map/1/poi should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/settings/map/1/poi')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });
  */

  test('Get map/1/poi should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map/1/poi')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });

  test('Get /template should 401', (done) => {
    server.start(() => {
      request(server.app)
        .get('/map/1/poi/1')
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
          done();
        });
    }, knex);
  });
});
