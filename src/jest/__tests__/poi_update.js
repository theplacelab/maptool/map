const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  testPoi,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { debug } = require('console');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: POI PATCH`, () => {
  test('Patch poi/ should update a poi', (done) => {
    server.start(() => {
      request(server.app)
        .put('/map/1/poi')
        .send({ ...testPoi[0] })
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const newRecord = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.CREATED);
          expect(newRecord.label).toBe(testPoi[0].label);
          request(server.app)
            .patch(`/map/${newRecord.map_id}/poi/${newRecord.id}`)
            .send({ ...testPoi[1] })
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then((response) => {
              const newRecord = JSON.parse(response.text);
              expect(response.statusCode).toEqual(RESPONSE.OK);
              expect(newRecord.label).toBe(testPoi[1].label);
              done();
            });
        });
    }, knex);
  });
});
