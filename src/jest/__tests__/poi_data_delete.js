const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  testPoi,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { debug } = require('console');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: POI_DATA Delete`, () => {
  test('Delete poi_data should delete a poi datum', (done) => {
    const value = faker.random.words();
    const bearerToken = `bearer ${generateTokenFor(testUsers[0])}`;
    server.start(() => {
      request(server.app)
        .put('/poi_data')
        .send({
          poi_id: '1',
          poi_data_options_id: '1',
          poi_data_format_id: '1',
          value,
          is_published: true,
          is_locked: true,
          is_markdown: true
        })
        .set('Authorization', bearerToken)
        .then((response) => {
          const newRecord = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.CREATED);
          expect(newRecord.value).toBe(value);
          request(server.app)
            .del(`/poi_data/${newRecord.id}`)
            .set('Authorization', bearerToken)
            .then((response) => {
              request(server.app)
                .get(`/poi_data/${newRecord.id}`)
                .set('Authorization', bearerToken)
                .then((response) => {
                  expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
                  done();
                });
            });
        });
    }, knex);
  });
});
