const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: Map CREATE`, () => {
  test(' Put map/ should create a map', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .put('/map/')
        .send({ ...testMaps[0], title })
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const newRecord = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.CREATED);
          expect(newRecord.title).toBe(title);
          done();
        });
    }, knex);
  });
});
