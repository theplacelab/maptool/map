const request = require('supertest');
const server = require('../../app');
const {
  testUsers,
  testPoi,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { debug } = require('console');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: POI (PLURAL) READ`, () => {
  test('Get poi/ should get all poi', (done) => {
    server.start(() => {
      request(server.app)
        .put('/map/1/poi')
        .send({ ...testPoi[0] })
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then(() => {
          request(server.app)
            .put('/map/1/poi')
            .send({ ...testPoi[1] })
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then(() => {
              request(server.app)
                .get('/map/1/poi')
                .set(
                  'Authorization',
                  `bearer ${generateTokenFor(testUsers[0])}`
                )
                .then((response) => {
                  const poi = JSON.parse(response.text);
                  expect(response.statusCode).toEqual(RESPONSE.OK);
                  expect(Array.isArray(poi));
                  done();
                });
            });
        });
    }, knex);
  });
});

describe(`API: POI READ`, () => {
  test('Get poi/:id should get :id poi', (done) => {
    server.start(() => {
      request(server.app)
        .put('/map/1/poi')
        .send({ ...testPoi[0] })
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then(() => {
          request(server.app)
            .put('/map/1/poi')
            .send({ ...testPoi[1] })
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then(() => {
              request(server.app)
                .get('/map/1/poi/1')
                .set(
                  'Authorization',
                  `bearer ${generateTokenFor(testUsers[0])}`
                )
                .then((response) => {
                  const poi = JSON.parse(response.text);
                  expect(response.statusCode).toEqual(RESPONSE.OK);
                  expect(poi.label).toBe(testPoi[0].label);
                  done();
                });
            });
        });
    }, knex);
  });
});
