const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: Map UPDATE`, () => {
  test(' Update map/ should update a map', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .patch('/map/0')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .send({ title })
        .then((response) => {
          const newRecord = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(newRecord.title).toBe(title);
          done();
        });
    }, knex);
  });

  test(' Update map/ with null should fail gracefully', (done) => {
    const title = faker.address.country();
    server.start(() => {
      request(server.app)
        .patch(`/map/null`)
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .send({ title })
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.BAD_REQUEST);
          done();
        });
    }, knex);
  });
});
