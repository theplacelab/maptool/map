const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex,
  settingsVersionNumber
} = require('../testSupport.js');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`TEMPLATES: Get template`, () => {
  test('Get global template', (done) => {
    server.start(() => {
      request(server.app)
        .get('/template/settings/global')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const template = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(template.version).toBe(`maptool_v${settingsVersionNumber}`);
          done();
        });
    }, knex);
  });

  test('Get map template', (done) => {
    server.start(() => {
      request(server.app)
        .get('/template/settings/map')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const template = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(template.version).toBe(`map_v${settingsVersionNumber}`);
          done();
        });
    }, knex);
  });

  test('Get poi template', (done) => {
    server.start(() => {
      request(server.app)
        .get('/template/settings/poi')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          const template = JSON.parse(response.text);
          expect(response.statusCode).toEqual(RESPONSE.OK);
          expect(template.version).toBe(`poi_v${settingsVersionNumber}`);
          done();
        });
    }, knex);
  });
});
