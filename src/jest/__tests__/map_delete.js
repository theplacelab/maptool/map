const request = require('supertest');
const server = require('../../app');
const { faker } = require('@faker-js/faker');
const {
  testUsers,
  testMaps,
  generateTokenFor,
  database,
  knex
} = require('../testSupport.js');
const { RESPONSE } = require('../../constants.js');

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(async () => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      const fs = require('fs');
      fs.unlink(database, () => {});
    })
  );
});

describe(`API: Map DELETE`, () => {
  test(' Delete map/ should delete a map', (done) => {
    server.start(() => {
      request(server.app)
        .del('/map/0')
        .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
        .then((response) => {
          expect(response.statusCode).toEqual(RESPONSE.OK);
          request(server.app)
            .get('/maps')
            .set('Authorization', `bearer ${generateTokenFor(testUsers[0])}`)
            .then((response) => {
              expect(Array.isArray(JSON.parse(response.text)));
              expect(JSON.parse(response.text).length).toEqual(0);
              request(server.app)
                .get('/map/0')
                .set(
                  'Authorization',
                  `bearer ${generateTokenFor(testUsers[0])}`
                )
                .then((response) => {
                  expect(response.statusCode).toEqual(RESPONSE.NOT_FOUND);
                });
              done();
            });
        });
    }, knex);
  });
});
