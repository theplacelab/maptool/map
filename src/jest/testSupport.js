const jwt = require('jsonwebtoken');
const { faker } = require('@faker-js/faker');
const { v4: uuidv4 } = require('uuid');
const jwtSecret = 'testsecret';
const parse = require('pg-connection-string').parse;
const fs = require('fs');
const path = require('path');
const os = require('os');
const debug = require('../modules/debugLog.js');
process.env.JWT_SECRET = jwtSecret;
const { RESPONSE, ROLE } = require('../constants.js');
const tempDir = fs.mkdtempSync(path.join(os.tmpdir(), 'map-service'));
const database = `${tempDir}/${uuidv4()}.db`;

const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: database
  },
  useNullAsDefault: true,
  migrations: {
    directory: __dirname + '/../data/migrations'
  },
  seeds: {
    directory: __dirname + '/seeds'
  }
});

module.exports = {
  knex,
  database,
  jwtSecret,
  settingsVersionNumber: '1.0',
  generateTokenFor: (user) => {
    return jwt.sign(
      {
        ...user,
        exp: Math.floor(Date.now() / 1000) + 1 * 60
      },
      jwtSecret
    );
  },
  token: jwt.sign(
    {
      role: ROLE.SUPER,
      exp: Math.floor(Date.now() / 1000) + 1 * 60
    },
    jwtSecret
  ),
  testUsers: [
    {
      name: faker.name.fullName(),
      email: faker.internet.email(),
      role: ROLE.USER,
      uuid: uuidv4(),
      f_reset: 0,
      f_valid: 1
    },
    {
      name: faker.name.fullName(),
      email: faker.internet.email(),
      role: ROLE.USER,
      uuid: uuidv4(),
      f_reset: 0,
      f_valid: 0
    }
  ],
  testPoi: [
    {
      map_id: 1,
      title: faker.address.city(),
      coordinates: JSON.stringify({
        lat: faker.address.latitude(),
        lng: faker.address.longitude()
      }),
      sequence: 1,
      poi_style_json: {}
    },
    {
      map_id: 1,
      title: faker.address.city(),
      coordinates: JSON.stringify({
        lat: faker.address.latitude(),
        lng: faker.address.longitude()
      }),
      sequence: 1,
      poi_style_json: {}
    }
  ],
  testMaps: [
    {
      title: faker.address.country(),
      uuid: uuidv4(),
      slug: faker.address.countryCode()
    },
    {
      title: faker.address.country(),
      uuid: uuidv4(),
      slug: faker.address.countryCode()
    },
    {
      title: faker.address.country(),
      uuid: uuidv4(),
      slug: faker.address.countryCode()
    }
  ]
};
