const { v4: uuidv4 } = require('uuid');
const poi_data_format_d = require('../../data/seeds/initialize/poi_data_format.json');
const map_d = require('../../data/seeds/initialize/maps.json');
const poi_data = require('../../data/seeds/initialize/poi_data.json');
const poi_style = require('../../data/seeds/initialize/poi_style.js');

exports.seed = async (knex) => {
  const seed = async (tableName, data) => {
    const existing = await knex.select('*').from(tableName);
    if (existing.length === 0) {
      const d = data.map((datum) => {
        if (Object.keys(datum).includes('uuid')) datum.uuid = uuidv4();
        if (Object.keys(datum).includes('created_at'))
          datum.created_at = new Date().toLocaleString('en-US');
        if (Object.keys(datum).includes('updated_at'))
          datum.updated_at = new Date().toLocaleString('en-US');
        return datum;
      });
      await knex(tableName).del();
      await knex(tableName).insert(d);
    }
  };

  try {
    seed('maps', map_d);
    seed('poi_data_format', poi_data_format_d);
    seed('poi_data', poi_data);
    seed('poi_style', poi_style);
  } catch (e) {
    console.debug(e);
  }
};
