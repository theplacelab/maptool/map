const STATUS = require('../constants/httpCodes.js');

module.exports = {
  read: async (knex, data, cb) => {
    const poi_style = await knex.select('*').from('poi_style');
    cb(STATUS.OK, JSON.stringify(poi_style ? poi_style : []));
  }
};
