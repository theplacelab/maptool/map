const STATUS = require('../constants/httpCodes.js');

module.exports = {
  create: (knex, data, cb) => {
    knex
      .insert(data)
      .into('poi_data')
      .returning('id')
      .then((latestInsert) => {
        const id = latestInsert[0].id ? latestInsert[0].id : latestInsert[0];
        knex('poi_data')
          .where('id', id)
          .then((newRecord) => {
            cb(STATUS.CREATED, newRecord[0]);
          })
          .catch((e) => cb(STATUS.BAD_REQUEST));
      })
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  read: async (knex, data, cb) => {
    const [poi_data] = await knex
      .select('*')
      .from('poi_data')
      .where({ id: data.id });
    cb(
      poi_data ? STATUS.OK : STATUS.NOT_FOUND,
      poi_data ? JSON.stringify(poi_data) : null
    );
  },

  update: async (knex, data, cb) => {
    await knex('poi_data')
      .where({ id: data.id })
      .update(data)
      .then(() => {
        knex('poi_data')
          .where({ id: data.id })
          .then((updatedRecord) => {
            cb(STATUS.OK, updatedRecord[0]);
          })
          .catch((e) => cb(STATUS.BAD_REQUEST));
      })
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  destroy: async (knex, data, cb) => {
    await knex('poi_data')
      .where({ id: data.id })
      .del()
      .then(() => cb(STATUS.OK))
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  reorder: async (knex, data, cb) => {
    data?.forEach(async (id, idx) => {
      await knex('poi_data')
        .where({ id })
        .update({
          sort_order: idx
        })
        .then(() => {
          if (idx === data.length - 1) cb(STATUS.OK);
        })
        .catch((e) => cb(STATUS.BAD_REQUEST));
    });
  }
};
