const STATUS = require('../constants/httpCodes.js');

module.exports = {
  read: async (knex, data, cb) => {
    const poi_data_format = await knex.select('*').from('poi_data_format');
    cb(STATUS.OK, JSON.stringify(poi_data_format ? poi_data_format : []));
  }
};
