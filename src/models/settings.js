const schema = require('../data/schema.js');
const STATUS = require('../constants/httpCodes.js');
const mapDefaultSettings = require('../settings/map.json');
const globalDefaultSettings = require('../settings/global.json');
const poiDefaultSettings = require('../settings/poi.json');

const merge = (settings, template) => {
  const mergedSettings = {};
  Object.keys(template).forEach((key) => {
    mergedSettings[key] = Object.keys(settings).includes(key)
      ? settings[key]
      : template[key];
  });
  return mergedSettings;
};

const mergedSettings = (knex, where, defaults, cb) => {
  knex('settings')
    .where(where)
    .then(async (records) => {
      const currentSettings = records[0]?.settings_json
        ? records[0].settings_json
        : {};
      const mergedSettings = merge(currentSettings, defaults);
      // Empty, record defaults
      let id;
      if (records.length === 0) {
        await knex
          .insert({ ...where, settings_json: JSON.stringify(mergedSettings) })
          .into('settings')
          .returning('id')
          .then((latestInsert) => {
            id = latestInsert[0].id ? latestInsert[0].id : latestInsert[0];
          })
          .catch((e) => cb(STATUS.BAD_REQUEST));
      } else {
        id = records[0].id ? records[0].id : records[0];
      }
      cb(mergedSettings, id);
    });
};

module.exports = {
  read: async (knex, data, cb) => {
    if (data.is_poi) {
      // POI per Map settings
      mergedSettings(
        knex,
        { map_id: data.id, is_poi: true },
        poiDefaultSettings,
        (settings) => cb(STATUS.OK, settings)
      );
    } else if (Object.keys(data).includes('id')) {
      // Map settings
      mergedSettings(
        knex,
        { map_id: data.id, is_poi: false },
        mapDefaultSettings,
        (settings) => cb(STATUS.OK, settings)
      );
    } else {
      // Global settings
      mergedSettings(
        knex,
        { map_id: null, is_poi: false },
        globalDefaultSettings,
        (settings) => cb(STATUS.OK, settings)
      );
    }
  },

  update: async (knex, data, cb) => {
    if (data.is_poi) {
      // POI per Map settings
      mergedSettings(
        knex,
        { map_id: data.id, is_poi: true },
        poiDefaultSettings,
        (settings, id) => {
          const updatedSettings = merge(data, settings);
          knex('settings')
            .where({ id })
            .update({ settings_json: JSON.stringify(updatedSettings) })
            .then(() => {
              cb(STATUS.OK, updatedSettings);
            })
            .catch((e) => cb(STATUS.BAD_REQUEST));
        }
      );
    } else if (Object.keys(data).includes('id')) {
      // Map settings
      mergedSettings(
        knex,
        { map_id: data.id, is_poi: false },
        mapDefaultSettings,
        (settings, id) => {
          const updatedSettings = merge(data, settings);
          knex('settings')
            .where({ id })
            .update({ settings_json: JSON.stringify(updatedSettings) })
            .then(() => {
              cb(STATUS.OK, updatedSettings);
            })
            .catch((e) => cb(STATUS.BAD_REQUEST));
        }
      );
    } else {
      // Global settings
      mergedSettings(
        knex,
        { map_id: null, is_poi: false },
        globalDefaultSettings,
        (settings, id) => {
          const updatedSettings = merge(data, settings);
          knex('settings')
            .where({ id })
            .update({ settings_json: JSON.stringify(updatedSettings) })
            .then(() => {
              cb(STATUS.OK, updatedSettings);
            })
            .catch((e) => cb(STATUS.BAD_REQUEST));
        }
      );
    }
  },

  style: async (knex, cb) => {
    const styleData = await knex.select('*').from('poi_style');
    cb(styleData ? STATUS.OK : STATUS.NOT_FOUND, styleData);
  },

  dataFormat: async (knex, cb) => {
    const dataFormat = await knex.select('*').from('poi_data_format');
    cb(dataFormat ? STATUS.OK : STATUS.NOT_FOUND, dataFormat);
  },

  readPoiConfig: async (knex, mapId, cb) => {
    const poiConfig = await knex
      .select('*')
      .from('poi_data_options')
      .where({ map_id: mapId })
      .orderBy('sort_order');
    cb(poiConfig ? STATUS.OK : STATUS.NOT_FOUND, poiConfig);
  },

  readPoiConfigById: async (knex, id, cb) => {
    const [poiConfig] = await knex
      .select('*')
      .from('poi_data_options')
      .where({ id });
    cb(poiConfig ? STATUS.OK : STATUS.NOT_FOUND, poiConfig);
  },
  createPoiConfig: async (knex, data, cb) => {
    const poiConfig = await knex
      .insert(data)
      .into('poi_data_options')
      .returning('*');
    cb(poiConfig ? STATUS.OK : STATUS.NOT_FOUND, poiConfig);
  },

  updatePoiConfig: async (knex, data, cb) => {
    await knex('poi_data_options')
      .where({ map_id: data.map_id, id: data.id })
      .update(data)
      .then(() => {
        knex('poi_data_options')
          .where({ id: data.id })
          .then((updatedRecord) => {
            cb(STATUS.OK, updatedRecord[0] ? updatedRecord[0] : {});
          })
          .catch((e) => cb(STATUS.BAD_REQUEST));
      })
      .catch((e) => {
        console.error(e);
        cb(STATUS.BAD_REQUEST);
      });
  },
  deletePoiConfig: async (knex, id, cb) => {
    const poiConfig = await knex('poi_data_options').where({ id }).del();
    cb(poiConfig ? STATUS.OK : STATUS.NOT_FOUND, poiConfig);
  },
  reorderPoiConfig: async (knex, data, cb) => {
    data?.forEach(async (id, idx) => {
      await knex('poi_data_options')
        .where({ id })
        .update({
          sort_order: idx
        })
        .then(() => {
          if (idx === data.length - 1) cb(STATUS.OK);
        })
        .catch((e) => cb(STATUS.BAD_REQUEST));
    });
  }
};
