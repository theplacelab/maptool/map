const schema = require('../data/schema.js');
const STATUS = require('../constants/httpCodes.js');
const { debug } = require('console');
const { parse } = require('path');
const { stringHasNumber } = require('../data/util.js');

module.exports = {
  create: async (knex, data, cb) => {
    const fields = await knex
      .select('*')
      .from('poi_data_options')
      .where({ map_id: data.map_id });
    const parsedData = {
      fields: fields?.map((f) => ({ [f.id]: '' })),
      poi: {}
    };
    Object.keys(data).forEach((key) => {
      if (Number(key)) {
        parsedData.fields[key] = data[key];
      } else {
        if (!stringHasNumber(key)) parsedData.poi[key] = data[key];
      }
    });
    knex('poi')
      .insert(parsedData.poi)
      .returning('id')
      .then((latestInsert) => {
        const id = latestInsert[0].id ? latestInsert[0].id : latestInsert[0];
        knex('poi')
          .where('id', id)
          .then((newRecord) => {
            const { id } = newRecord[0];
            parsedData.fields.forEach(async (f) => {
              const fieldId = Number(Object.keys(f)[0]);
              const fieldValue = parsedData.fields[fieldId];
              const field = fields.find((i) => i.id === fieldId);
              if (field) {
                const d = {
                  poi_id: id,
                  poi_data_options_id: fieldId,
                  value: fieldValue,
                  is_published:
                    typeof data[`${fieldId}_published`] !== 'undefined'
                      ? data[`${fieldId}_published`]
                      : field.is_published_default
                      ? field.is_published_default
                      : false,
                  is_locked:
                    typeof data[`${fieldId}_locked`] !== 'undefined'
                      ? data[`${fieldId}_locked`]
                      : field.is_locked_default
                      ? field.is_locked_default
                      : false,
                  is_markdown:
                    typeof data[`${fieldId}_markdown`] !== 'undefined'
                      ? data[`${fieldId}_markdown`]
                      : field.is_markdown_default
                      ? field.is_markdown_default
                      : false
                };
                knex('poi_data')
                  .insert(d)
                  .catch((e) => console.error(e));
              }
            });
            cb(STATUS.CREATED, newRecord[0]);
          })
          .catch((e) => {
            console.error(e);
            cb(STATUS.BAD_REQUEST);
          });
      })
      .catch((e) => {
        console.error(e);
        cb(STATUS.BAD_REQUEST);
      });
  },

  read: async (knex, data, cb) => {
    try {
      const records = await knex('poi')
        .select(
          'poi.*',
          'poi_data.poi_data_options_id',
          'poi_data.value',
          'poi_data.is_published',
          'poi_data.is_locked',
          'poi_data.is_markdown'
        )
        .where({ map_id: data.map_id })
        .leftJoin('poi_data', 'poi_data.poi_id', 'poi.id')
        .modify((q) => {
          if (data.poi_id) q.where('poi.id', data.poi_id);
        })
        .orderBy('sort_order');

      let filteredRecords = [];
      records.forEach((record) => {
        const found = filteredRecords.find((r) => Number(r.id) === record.id);
        const recordData = record.poi_data_options_id
          ? {
              poi_data_options_id: record.poi_data_options_id,
              poi_data_format_id: record.poi_data_format_id,
              value: record.value,
              is_published: record.is_published,
              is_locked: record.is_locked,
              is_markdown: record.is_markdown
            }
          : null;
        if (!found) {
          const rec = {
            id: record.id,
            map_id: record.map_id,
            slug: record.slug,
            title: record.title,
            description: record.description,
            coordinates: record.coordinates,
            sequence: record.sequence,
            poi_style_json: record.poi_style_json,
            created_by: record.created_by,
            created_at: record.created_at,
            updated_at: record.updated_at,
            fields: []
          };
          if (recordData) rec.fields = [recordData];
          filteredRecords.push(rec);
        } else {
          found.fields.push(recordData ? recordData : null);
        }
      });
      cb(
        STATUS.OK,
        filteredRecords.length ? filteredRecords : filteredRecords[0]
      );
    } catch (e) {
      console.error(e.message);
      cb(STATUS.BAD_REQUEST);
    }
  },

  update: async (knex, data, cb) => {
    const fields = await knex
      .select('*')
      .from('poi_data_options')
      .where({ map_id: data.map_id });
    const parsedData = {
      fields: fields?.map((f) => ({ [f.id]: '' })),
      poi: {}
    };
    Object.keys(data).forEach((key) => {
      if (Number(key)) {
        parsedData.fields[key] = data[key];
      } else {
        if (!stringHasNumber(key)) parsedData.poi[key] = data[key];
      }
    });

    console.log(parsedData);
    knex('poi')
      .where({ id: data.id })
      .update(parsedData.poi)
      .then((record) => {
        parsedData.fields.forEach(async (f) => {
          if (!f) return;
          const fieldId = Number(Object.keys(f)[0]);
          const fieldValue = parsedData.fields[fieldId];
          const field = fields.find((i) => i.id === fieldId);
          if (field) {
            const d = {
              poi_id: data.id,
              poi_data_options_id: fieldId,
              value: fieldValue,
              is_published:
                typeof data[`${fieldId}_published`] !== 'undefined'
                  ? data[`${fieldId}_published`]
                  : field.is_published_default
                  ? field.is_published_default
                  : false,
              is_locked:
                typeof data[`${fieldId}_locked`] !== 'undefined'
                  ? data[`${fieldId}_locked`]
                  : field.is_locked_default
                  ? field.is_locked_default
                  : false,
              is_markdown:
                typeof data[`${fieldId}_markdown`] !== 'undefined'
                  ? data[`${fieldId}_markdown`]
                  : field.is_markdown_default
                  ? field.is_markdown_default
                  : false
            };
            knex('poi_data')
              .where({ poi_id: data.id, poi_data_options_id: fieldId })
              .update({ value: fieldValue })
              .catch((e) => {
                console.error(e);
                cb(STATUS.BAD_REQUEST);
              });
          }
        });
        cb(STATUS.CREATED, record[0]);
      })
      .catch((e) => {
        console.error(e);
        cb(STATUS.BAD_REQUEST);
      });
  },

  destroy: async (knex, data, cb) => {
    await knex('poi')
      .where({ map_id: data.map_id, id: data.poi_id })
      .del()
      .then(() => cb(STATUS.OK))
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  reorder: async (knex, data, cb) => {
    data?.forEach(async (id, idx) => {
      await knex('poi')
        .where({ id })
        .update({
          sort_order: idx
        })
        .then(() => {
          if (idx === data.length - 1) cb(STATUS.OK);
        })
        .catch((e) => {
          console.error(e);
          cb(STATUS.BAD_REQUEST);
        });
    });
  }
};
