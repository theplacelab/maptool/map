const { v4: uuidv4 } = require('uuid');
const STATUS = require('../constants/httpCodes.js');
const { debug } = require('console');

_validSlug = async (knex, data) => {
  const { id, title, slug } = data;
  let proposedSlug = slug;
  if (!proposedSlug || proposedSlug?.trim().length === 0) proposedSlug = title;

  proposedSlug = proposedSlug
    .trim()
    .toLowerCase()
    .replaceAll('-', ' ')
    .replace(/[^a-zA-Z0-9 ]/g, '')
    .replaceAll(' ', '-');

  let x = 2;
  let found = true;
  while (found) {
    let result = id
      ? await knex('maps').where({ slug: proposedSlug }).andWhereNot({ id })
      : await knex('maps').where({ slug: proposedSlug });
    if (result.length > 0) {
      proposedSlug = `${proposedSlug.replace(/[0-9]/g, '')}${x}`;
      x++;
    }
    found = result.length > 0;
  }
  return proposedSlug;
};

module.exports = {
  create: async (knex, data, cb) => {
    const map = {
      ...data,
      slug: await _validSlug(knex, data),
      uuid: uuidv4(),
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString()
    };
    knex
      .insert(map)
      .into('maps')
      .returning('id')
      .then((latestInsert) => {
        const id = latestInsert[0].id ? latestInsert[0].id : latestInsert[0];
        knex('maps')
          .where('id', id)
          .then((newRecord) => cb(STATUS.CREATED, newRecord[0]))
          .catch((e) => cb(STATUS.BAD_REQUEST));
      })
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  read: async (knex, data, cb) => {
    if (data.id === '*') {
      const maps = await knex
        .select('maps.*', 'settings.settings_json')
        .from('maps')
        .leftJoin('settings', 'settings.map_id', 'maps.id')
        .orderBy('sort_order');
      cb(STATUS.OK, JSON.stringify(maps ? maps : []));
    } else {
      const [map] = await knex.select('*').from('maps').where({ id: data.id });
      cb(map ? STATUS.OK : STATUS.NOT_FOUND, map ? JSON.stringify(map) : null);
    }
  },

  update: async (knex, data, cb) => {
    await knex('maps')
      .where({ id: data.id })
      .update({
        ...data,
        slug: await _validSlug(knex, data),
        updated_at: new Date().toISOString()
      })
      .then(() => {
        knex('maps')
          .where({ id: data.id })
          .then((updatedRecord) => {
            cb(STATUS.OK, updatedRecord[0]);
          })
          .catch((e) => cb(STATUS.BAD_REQUEST));
      })
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  destroy: async (knex, data, cb) => {
    await knex('maps')
      .where({ id: data.id })
      .del()
      .then(() => cb(STATUS.OK))
      .catch((e) => cb(STATUS.BAD_REQUEST));
  },

  reorder: async (knex, data, cb) => {
    data?.forEach(async (id, idx) => {
      await knex('maps')
        .where({ id })
        .update({
          sort_order: idx
        })
        .then(() => {
          if (idx === data.length - 1) cb(STATUS.OK);
        })
        .catch((e) => cb(STATUS.BAD_REQUEST));
    });
  }
};
