'use strict';
require('dotenv').config({ path: require('find-config')('.env') });
const express = require('express');
const cors = require('cors');
const jwt = require('./modules/jwt.js');
const debug = require('./modules/debugLog.js');
const env = require('./modules/environment.js');
const bodyParser = require('body-parser');

const base = require('./routes/base.js');
const map = require('./routes/map.js');
const maps = require('./routes/maps.js');
const settings = require('./routes/settings.js');
const template = require('./routes/template.js');
const poi_data = require('./routes/poi_data.js');

const knexConfig = require('./data/knexfile.js');
const app = express();
const { RESPONSE } = require('./constants.js');

module.exports = {
  app,
  start: (cb, overrideKnex, shouldSeed = false) => {
    const knex = overrideKnex ? overrideKnex : require('knex')(knexConfig);
    if (env.validate()) {
      knex.migrate.latest().then(() => {
        const configApi = () => {
          app.use(cors());
          app.use(bodyParser.json());
          app.use((err, _req, res, next) => {
            if (!err) next();
            console.error(err);
            res.status(RESPONSE.UNPROCESSABLE).send({ error: 'Invalid JSON' });
          });
          app.use((req, _res, next) => {
            req.knex = knex;
            next();
          });

          app.use('/', debug);
          app.use('/', base);

          app.use('/map', jwt.validate);
          app.use('/map', map);

          //app.use('/settings', jwt.validate);
          app.use('/settings', settings);

          //app.use('/maps', jwt.validate);
          app.use('/maps', maps);

          app.use('/poi_data', jwt.validate);
          app.use('/poi_data', poi_data);

          app.use('/template', jwt.validate);
          app.use('/template', template);

          app.use((req, res) => {
            debug.log(`REQ: 404 - No Route`);
            res.sendStatus(RESPONSE.NOT_FOUND);
          });

          env.display();
          if (typeof cb === 'function') cb(knex);
        };
        if (shouldSeed) {
          knex.seed.run().then(async () => {
            const users = await knex.select('*').from('users');
            configApi();
          });
        } else {
          configApi();
        }
      });
    }
  }
};
