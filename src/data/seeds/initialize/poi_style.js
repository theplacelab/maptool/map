const MARKER = {
  CIRCLE: 'CIRCLE',
  IMAGE: 'IMAGE',
  PIN: 'PIN'
};

module.exports = [
  {
    display_name: 'Pin',
    slug: MARKER.PIN,
    properties_json: { size: '2rem' }
  },
  {
    display_name: 'Circle',
    slug: MARKER.CIRCLE,
    properties_json: {
      size: '2rem',
      fill: '#FFFF00',
      border: '5px solid #FF00FF'
    }
  },
  {
    display_name: 'Image',
    slug: MARKER.IMAGE,
    properties_json: { size: '10rem', image: '' }
  }
];
