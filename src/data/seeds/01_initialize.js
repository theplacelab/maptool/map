const { v4: uuidv4 } = require('uuid');
const poi_data_format_d = require('./initialize/poi_data_format.json');
const maps_d = require('./initialize/maps.json');
const poi_data = require('./initialize/poi_data.json');
const poi_style = require('../../data/seeds/initialize/poi_style.js');

exports.seed = async (knex) => {
  const seedIfEmpty = async (tableName, data) => {
    const existing = await knex.select('*').from(tableName);
    if (existing.length === 0) {
      const d = data.map((datum) => {
        if (Object.keys(datum).includes('uuid')) datum.uuid = uuidv4();
        if (Object.keys(datum).includes('created_at'))
          datum.created_at = new Date().toLocaleString('en-US');
        if (Object.keys(datum).includes('updated_at'))
          datum.updated_at = new Date().toLocaleString('en-US');
        return datum;
      });
      console.log(`SEEDING: ${tableName}`);
      await knex(tableName).insert(d);
    }
  };

  try {
    seedIfEmpty('maps', maps_d);
    seedIfEmpty('poi_data_format', poi_data_format_d);
    seedIfEmpty('poi_data', poi_data);
    seedIfEmpty('poi_style', poi_style);
  } catch (e) {
    console.debug(e);
  }
};
