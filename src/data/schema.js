// prettier-ignore
module.exports = {
  tables:{
    maps: {
      title:                    { type: "text",                     nullable: true },
      slug:                     { type: "text",     unique: true,   nullable: false},
      uuid:                     { type: "string",   unique: true,   nullable: false, validations: {isUUID: true}},
      created_by:               { type: "string",   nullable: true, validations: { isUUID: true }},
      created_at:               { type: "dateTime", nullable: false },
      updated_at:               { type: "dateTime", nullable: false },
      style_json:               { type: "jsonb",                    nullable: true },
      sort_order:               { type: "integer",                  nullable: true },
    },
    poi:{
      map_id:                   { type: "integer",  nullable: true },
      slug:                     { type: "text",     nullable: true },
      title:                    { type: "text",     nullable: true },
      description:              { type: "text",     nullable: true },
      coordinates:              { type: "jsonb",     nullable: true },
      sort_order:               { type: "text",     nullable: true },
      poi_style_json:           { type: "jsonb",    nullable: true },
      created_by:               { type: "string",   nullable: true, validations: { isUUID: true }},
      created_at:               { type: "dateTime", nullable: false },
      updated_at:               { type: "dateTime", nullable: false }
    },
    poi_data:{
      poi_id:                   { type: "integer",  nullable: true },
      poi_data_options_id:      { type: "integer",  nullable: true },
      value:                    { type: "text",     nullable: true },
      is_published:             { type: "boolean",  nullable: false,  default:"true" },
      is_locked:                { type: "boolean",  nullable: false,  default:"true" },
      is_markdown:              { type: "boolean",  nullable: false,  default:"true" },
    },
    poi_data_options:{
      label:                    { type: "text",     nullable: true },
      search_scope:             { type: "text",     nullable: true },
      is_published_default:     { type: "boolean",  nullable: true },
      is_searchable:            { type: "boolean",  nullable: true },
      is_locked_default:        { type: "boolean",  nullable: true },
      is_markdown_default:      { type: "boolean",  nullable: true },
      map_id:                   { type: "integer",  nullable: true },
      sort_order:               { type: "integer",  nullable: true },
      poi_data_format_id:       { type: "integer",  nullable: true },
    },
    poi_data_format:{
      display_name:             { type: "text",     nullable: false },
      slug:                     { type: "text",     unique: true,   nullable: false }
    },
    poi_style:{
      display_name:             { type: "text",     nullable: false },
      slug:                     { type: "text",     nullable: false },
      properties_json:          { type: "jsonb",    nullable: true }
    },
    settings:{
      map_id:                   { type: "integer",  nullable: true },
      is_poi:                   { type: "boolean",  nullable: true,   default:false },
      settings_json:            { type: "jsonb",    nullable: true }
    }
  }
};
