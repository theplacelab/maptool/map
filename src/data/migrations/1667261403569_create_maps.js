const { create } = require('../../models/map.js');
const dbSchema = require('../schema.js');
const { createTable } = require('../util.js');

exports.up = function (knex) {
  return createTable(knex, 'maps');
};

exports.down = function (knex) {
  return knex.schema.dropTable('maps');
};
