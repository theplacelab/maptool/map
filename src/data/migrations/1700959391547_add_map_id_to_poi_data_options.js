exports.up = function (knex) {
  return knex.schema.table('poi_data_options', (table) => {
    table.integer('map_id').notNullable();
    table.foreign('map_id').references('maps.id');
  });
};

exports.down = function (knex) {
  return knex.schema.table('poi_data_options', (table) => {
    table.dropColumn('map_id');
  });
};
