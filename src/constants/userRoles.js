module.exports = {
  ANON: ROLE.ANON,
  USER: ROLE.USER,
  SUPER: ROLE.SUPER
};
