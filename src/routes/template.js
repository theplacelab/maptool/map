const router = require('express').Router();
const mapDefaultSettings = require('../settings/map.json');
const globalDefaultSettings = require('../settings/global.json');
const poiDefaultSettings = require('../settings/poi.json');
const STATUS = require('../constants/httpCodes.js');

router.get('/settings/global', (req, res) => {
  res.status(STATUS.OK).send(globalDefaultSettings);
});

router.get('/settings/map', (req, res) => {
  res.status(STATUS.OK).send(mapDefaultSettings);
});

router.get('/settings/poi', (req, res) => {
  res.status(STATUS.OK).send(poiDefaultSettings);
});

module.exports = router;
