const router = require('express').Router();
var map = require('../models/map');
const {
  requireValidToken,
  requireIntInParams
} = require('../modules/validation');
const settings = require('../models/settings');

// Global
router.get('/', (req, res) => {
  const { mapId } = req.params;
  settings.read(req.knex, { mapId }, (code, response) => {
    res.status(code).send(response);
  });
});

router.patch('/', (req, res) => {
  settings.update(req.knex, { ...req.body }, (code, response) => {
    res.status(code).send(response);
  });
});

// Constants
router.get('/poi-style', (req, res) => {
  settings.style(req.knex, (code, response) => {
    res.status(code).send(response);
  });
});

router.get('/poi-data-format', (req, res) => {
  settings.dataFormat(req.knex, (code, response) => {
    res.status(code).send(response);
  });
});

// POI Configuration
router.get('/map/:mapId/poi/config', (req, res) => {
  requireIntInParams(['mapId'], req, res, () => {
    const { mapId } = req.params;
    settings.readPoiConfig(req.knex, mapId, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.get('/map/:mapId/poi/config/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const { id } = req.params;
    settings.readPoiConfigById(req.knex, id, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.put('/map/:mapId/poi/config', (req, res) => {
  requireIntInParams(
    ['mapId', 'poi_data_format_id', 'sort_order'],
    req,
    res,
    () => {
      const data = { map_id: req.params.mapId, ...req.body };
      settings.createPoiConfig(req.knex, data, (code, response) => {
        res.status(code).send(response);
      });
    }
  );
});
router.post('/map/:mapId/poi/config/order', (req, res) => {
  requireIntInParams(['mapId'], req, res, () => {
    settings.reorderPoiConfig(req.knex, [...req.body], (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.patch('/map/:mapId/poi/config/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const data = { map_id: req.params.mapId, ...req.body };
    settings.updatePoiConfig(req.knex, data, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.delete('/map/:mapId/poi/config/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const { id } = req.params;
    settings.deletePoiConfig(req.knex, id, (code, response) => {
      res.status(code).send(response);
    });
  });
});

// Map Settings
router.get('/map/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const { id } = req.params;
    settings.read(req.knex, { is_poi: false, id }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.patch('/map/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const { id } = req.params;
    settings.update(
      req.knex,
      { is_poi: false, id, ...req.body },
      (code, response) => {
        res.status(code).send(response);
      }
    );
  });
});

// Deprecate?
/*
router.get('/map/:id/poi', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const { id } = req.params;
    settings.read(req.knex, { is_poi: true, id }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.patch('/map/:id/poi', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const { id } = req.params;
    settings.update(
      req.knex,
      { is_poi: true, id, ...req.body },
      (code, response) => {
        res.status(code).send(response);
      }
    );
  });
});
*/

module.exports = router;
