const router = require('express').Router();
const { requireIntInParams } = require('../modules/validation.js');
const map = require('../models/map.js');
const poi = require('../models/poi.js');
const settings = require('../models/settings.js');
//const { roles: ROLE } = require('../modules/constUserRoles.js');
const STATUS = require('../constants/httpCodes.js');

// POI
router.get('/:map_id/poi/:poi_id', (req, res) => {
  requireIntInParams(['map_id', 'poi_id'], req, res, () => {
    const { poi_id, map_id } = req.params;
    poi.read(req.knex, { poi_id, map_id }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.patch('/:map_id/poi/:id', (req, res) => {
  requireIntInParams(['map_id', 'id'], req, res, () => {
    const { id, map_id } = req.params;
    poi.update(req.knex, { id, map_id, ...req.body }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.delete('/:map_id/poi/:poi_id', (req, res) => {
  requireIntInParams(['map_id', 'poi_id'], req, res, () => {
    const { poi_id, map_id } = req.params;
    poi.destroy(req.knex, { poi_id, map_id }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.get('/:map_id/poi/', (req, res) => {
  requireIntInParams(['map_id'], req, res, () => {
    const { map_id } = req.params;
    poi.read(req.knex, { map_id }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.post('/:map_id/poi/order', (req, res) => {
  requireIntInParams(['map_id'], req, res, () => {
    console.log(req.body);
    poi.reorder(req.knex, [...req.body], (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.put('/:map_id/poi/', (req, res) => {
  requireIntInParams(['map_id'], req, res, () => {
    const { map_id } = req.params;
    poi.create(
      req.knex,
      {
        map_id,
        created_by: req.token.uuid,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString(),
        ...req.body
      },
      (code, response) => {
        res.status(code).send(response);
      }
    );
  });
});

// Map
router.get('/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const id = parseInt(req.params.id, 10);
    map.read(req.knex, { id }, (code, response) => {
      if (!response) {
        res.status(code).send(response);
      } else {
        settings.read(req.knex, { id }, (code, settings) => {
          response = JSON.stringify({ ...JSON.parse(response), settings });
          res.status(code).send(response);
        });
      }
    });
  });
});

router.put('/', (req, res) => {
  const data = { created_by: req.token.uuid, ...req.body };
  const mapSettings = { ...data.settings };
  delete data.settings;

  map.create(req.knex, data, (code, mapResponse) => {
    if (code === STATUS.CREATED) {
      settings.update(
        req.knex,
        { id: mapResponse.id, ...mapSettings },
        (_, settingsResponse) => {
          res.status(STATUS.CREATED).send({
            ...mapResponse,
            settings: { ...settingsResponse }
          });
        }
      );
    } else {
      res.status(code).send(mapResponse);
    }
  });
});

router.delete('/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    map.destroy(req.knex, { id: req.params.id }, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.patch('/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const data = { id: req.params.id, ...req.body };
    const mapSettings = { ...data.settings };
    delete data.settings;
    map.update(req.knex, data, (code, mapRes) => {
      settings.update(
        req.knex,
        { ...mapSettings, id: req.params.id },
        (code, settingsRes) => {
          res.status(code).send({ ...mapRes, settings: { ...settingsRes } });
        }
      );
    });
  });
});

module.exports = router;
