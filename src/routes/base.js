const router = require('express').Router();
const { RESPONSE } = require('../constants.js');

router.get('/up', (req, res) => {
  res.status(RESPONSE.OK).send('MAP');
});

module.exports = router;
