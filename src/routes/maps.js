const router = require('express').Router();
var map = require('../models/map');
const { requireValidToken } = require('../modules/validation');

router.get('/', (req, res) => {
  map.read(req.knex, { id: '*' }, (code, response) => {
    res.status(code).send(response);
  });
});

router.post('/order', (req, res) => {
  map.reorder(req.knex, [...req.body], (code, response) => {
    res.status(code).send(response);
  });
});

module.exports = router;
