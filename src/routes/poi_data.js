const router = require('express').Router();
const { requireIntInParams } = require('../modules/validation');
const poi_data = require('../models/poi_data');

router.put('/', (req, res) => {
  const data = { ...req.body };
  poi_data.create(req.knex, data, (code, response) => {
    res.status(code).send(response);
  });
});

router.get('/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const data = { id: req.params.id, ...req.body };
    poi_data.read(req.knex, data, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.patch('/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const data = { id: req.params.id, ...req.body };
    poi_data.update(req.knex, data, (code, response) => {
      res.status(code).send(response);
    });
  });
});

router.delete('/:id', (req, res) => {
  requireIntInParams(['id'], req, res, () => {
    const data = { id: req.params.id };
    poi_data.destroy(req.knex, data, (code, response) => {
      res.status(code).send(response);
    });
  });
});

module.exports = router;
