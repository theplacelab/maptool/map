# Map Service


### Map
| METHOD | ENDPOINT                  | BEARER              |
|:-------|---------------------------|---------------------|
| GET    | maps/                     | 🔒 auth token       |

| GET    | map/:id                   | 🔒 auth token       |
| PUT    | map/                      | 🔒 auth token       |
| DELETE | map/:id                   | 🔒 auth token       |
| PATCH  | map/:id                   | 🔒 auth token       |
 
| GET    | map/settings              | 🔒 auth token       |
| PATCH  | map/settings              | 🔒 auth token       |

| GET    | map/:id/settings          | 🔒 auth token       |
| PATCH  | map/:id/settings          | 🔒 auth token       |

| GET    | map/:id/poi/settings      | 🔒 auth token       |
| PATCH  | map/:id/poi/settings      | 🔒 auth token       |

| GET    | map/:id/poi/              | 🔒 auth token       |
| GET    | map/:id/poi/:id           | 🔒 auth token       |
| PUT    | map/:id/poi/              | 🔒 auth token       |
| DELETE | map/:id/poi/:id           | 🔒 auth token       |
| PATCH  | map/:id/poi/:id           | 🔒 auth token       |


| GET    | template/settings/global  | 🔒 auth token       |
| GET    | template/settings/map     | 🔒 auth token       |
| GET    | template/settings/poi     | 🔒 auth token       |

| GET    | resource/poi_data_format  | 🔒 auth token       |
| GET    | resource/poi_style        | 🔒 auth token       |

| GET    | poi_data/:id              | 🔒 auth token       |
| PUT    | poi_data/                 | 🔒 auth token       |
| DELETE | poi_data/:id              | 🔒 auth token       |
| PATCH  | poi_data/:id              | 🔒 auth token       |

```sh
DEBUG=true
PORT=8888
JWT_SECRET=
```


## Templates
    - Based on a merge of current settings with json files on the server
    - Base template can be overwtitten using k8
    - Include version number

## Resource
    - Stored in database (so managed with seed and migration) but read-only because they require companion UI work to support additonal